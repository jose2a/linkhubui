﻿using BOL;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class URLDb
    {
        private LinkHubDbEntities _db;

        public URLDb()
        {
            _db = new LinkHubDbEntities();
        }
        public IEnumerable<tbl_Url> GetAll()
        {
            return _db.tbl_Url.ToList();
        }
        public tbl_Url GetByID(int id)
        {
            return _db.tbl_Url.Find(id);
        }
        public void Insert(tbl_Url url)
        {
            _db.tbl_Url.Add(url);
            Save();
        }
        public void Delete(int id)
        {
            tbl_Url url = _db.tbl_Url.Find(id);
            _db.tbl_Url.Remove(url);
            Save();
        }
        public void Update(tbl_Url url)
        {
            _db.Entry(url).State = EntityState.Modified;
        }
        private void Save()
        {
            _db.SaveChanges();
        }

    }
}
