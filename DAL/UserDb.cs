﻿using BOL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class UserDb
    {
        private LinkHubDbEntities _db;
        public UserDb()
        {
            _db = new LinkHubDbEntities();
        }
        public IEnumerable<tbl_User> GetAll()
        {
            return _db.tbl_User.ToList();
        }
        public tbl_User GetByID(int id)
        {
            return _db.tbl_User.Find(id);
        }
        public void Insert(tbl_User user)
        {            
            _db.tbl_User.Add(user);
            Save();
        }
        public void Delete(int id)
        {
            _db.tbl_User.Remove(GetByID(id));
            Save();
        }
        public void Update(tbl_User user)
        {
            _db.Entry(user).State = System.Data.Entity.EntityState.Modified;
        }

        private void Save()
        {
            _db.SaveChanges();
        }
    }
}
