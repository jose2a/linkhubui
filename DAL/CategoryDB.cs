﻿using BOL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class CategoryDB
    {
        private LinkHubDbEntities _db;
        public CategoryDB()
        {
            _db = new LinkHubDbEntities();
        }
        public IEnumerable<tbl_Category> GetAll()
        {
            return _db.tbl_Category.ToList();
        }
        public tbl_Category GetByID(int id)
        {
            return _db.tbl_Category.Find(id);
        }
        public void Insert(tbl_Category category)
        {            
            _db.tbl_Category.Add(category);
            Save();
        }
        public void Delete(int id)
        {
            _db.tbl_Category.Remove(GetByID(id));
            Save();
        }
        public void Update(tbl_Category category)
        {
            _db.Entry(category).State = System.Data.Entity.EntityState.Modified;
        }

        private void Save()
        {
            _db.SaveChanges();
        }
    }
}
