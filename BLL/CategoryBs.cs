﻿using BOL;
using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class CategoryBs
    {
        private CategoryDB _categoryDb;
        public CategoryBs()
        {
            _categoryDb = new CategoryDB();
        }
        public IEnumerable<tbl_Category> GetAll()
        {
            return _categoryDb.GetAll();
        }
        public tbl_Category GetByID(int id)
        {
            return _categoryDb.GetByID(id);
        }
        public void Insert(tbl_Category category)
        {
            _categoryDb.Insert(category);
        }
        public void Delete(int id)
        {
            _categoryDb.Delete(id);
        }
        public void Update(tbl_Category category)
        {
            _categoryDb.Update(category);
        }
    }
}
