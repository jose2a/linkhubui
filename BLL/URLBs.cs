﻿using BOL;
using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class URLBs
    {
        private URLDb _urlDb;
        public URLBs()
        {
            _urlDb = new URLDb();
        }
        public IEnumerable<tbl_Url> GetAll()
        {
            return _urlDb.GetAll();
        }
        public tbl_Url GetByID(int id)
        {
            return _urlDb.GetByID(id);
        }
        public void Insert(tbl_Url url)
        {
            _urlDb.Insert(url);
        }
        public void Delete(int id)
        {
            _urlDb.Delete(id);
        }
        public void Update(tbl_Url url)
        {
            _urlDb.Update(url);
        }
    }
}
