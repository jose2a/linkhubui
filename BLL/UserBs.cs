﻿using BOL;
using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class UserBs
    {
        private UserDb _userDb;
        public UserBs()
        {
            _userDb = new UserDb();
        }
        public IEnumerable<tbl_User> GetAll()
        {
            return _userDb.GetAll();
        }
        public tbl_User GetByID(int id)
        {
            return _userDb.GetByID(id);
        }
        public void Insert(tbl_User user)
        {
            _userDb.Insert(user);
        }
        public void Delete(int id)
        {
            _userDb.Delete(id);
        }
        public void Update(tbl_User user)
        {
            _userDb.Update(user);
        }
    }
}
